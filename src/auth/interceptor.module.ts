import {
  HTTP_INTERCEPTORS,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
} from '@angular/common/http';
import { Injectable, NgModule } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { LoginProvider } from '../providers/login/login';

@Injectable()

export class HttpsRequestInterceptor implements HttpInterceptor {

  private static readonly BACKEND_URL = 'http://api.rtairqual.com.br/api';

  constructor(
    private loginProvider: LoginProvider
  ) { }

  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {

    let headers = req.headers.set('Content-Type', 'application/json');
    if (this.loginProvider.user) {
      headers = req.headers.set('X-User-Email', this.loginProvider.user.email)
        .set('X-User-Token', this.loginProvider.user.token);
    }

    const dupReq = req.clone({
      headers,
      url: HttpsRequestInterceptor.BACKEND_URL + req.url,
    });

    return next.handle(dupReq);
  }
}

@NgModule({
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpsRequestInterceptor,
      multi: true,
    },
  ],
})
export class Interceptor { }
