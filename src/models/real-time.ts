export interface RealTime {
  ponto: number;
  temperatura: number;
  umidade: number;
  qualidade_ar: number;
  chuva: number;
  dica_temperatura: string;
  dica_umidade: string;
  dica_qualidade_ar: string;
}
