export interface User {
  id: number;
  name: string;
  email: string;
  token: string;
  avatar: Avatar;
  dias_acesso: number;
  assinante: boolean;
  messagem?: any;
}

interface Avatar {
  url: string;
  normal: Image;
  thumb: Image;
  mini: Image;
}

interface Image {
  url: string;
}
