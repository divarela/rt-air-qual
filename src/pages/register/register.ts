import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { IonicPage, NavController } from 'ionic-angular';

import { Team } from '../../models/team';
import { CameraProvider } from '../../providers/camera/camera';
import { RegisterProvider } from '../../providers/register/register';
import { ServerValidationProvider } from '../../providers/server-validation/server-validation';
import { TeamProvider } from '../../providers/team/team';

@IonicPage()
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  form: FormGroup;
  teams: Team[] = [];
  loading: boolean;
  serverError: string;

  constructor(
    private serverValidation: ServerValidationProvider,
    private teamProvider: TeamProvider,
    private navController: NavController,
    private cameraProvider: CameraProvider,
    private registerProvider: RegisterProvider,
    formBuilder: FormBuilder
  ) {
    this.form = formBuilder.group(
      {
        avatar: ['', Validators.required],
        nome: ['', Validators.compose([Validators.required, Validators.minLength(3)])],
        email: ['', Validators.compose([Validators.required, Validators.email]), this.serverValidation.emailValid.bind(this.serverValidation)],
        password: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
        confirmarSenha: [''],
        genero: ['', Validators.required],
        telefone: ['', Validators.compose([Validators.required, Validators.minLength(14), Validators.maxLength(15)])],
        altura: ['', Validators.compose([Validators.min(0.50), Validators.max(3)])],
        peso: ['', Validators.compose([Validators.min(10), Validators.max(500), Validators.maxLength(3)])],
        equipe: [''],
        frequencia_atividade: ['', Validators.required],
      },
      {
        validator: this.serverValidation.mismatchedPasswords('password', 'confirmarSenha'),
      }
    );
  }

  ionViewWillEnter() {
    this.teamProvider.getAll()
      .subscribe(teams => this.teams = teams);
  }

  async cancel() {
    try {
      await this.navController.pop();
    } catch (error) {
      console.error('[RegisterPage] cancel', error);
    }
  }

  async getUserImage() {
    try {
      const picture = await this.cameraProvider.getPicture();
      this.form.controls['avatar'].setValue(picture);
    } catch (error) {
      console.error('[RegisterPage] getUserImage', error);
    }
  }

  register() {
    this.serverError = '';
    this.loading = true;

    this.registerProvider.post(this.form.value)
      .subscribe(
        () => this.navController.setRoot('MenuPage'),
        error => {
          this.loading = false;
          this.serverError = error.error.error;
        }
      );
  }

}
