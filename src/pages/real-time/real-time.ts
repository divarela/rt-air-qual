import { Component } from '@angular/core';
import { IonicPage, NavParams, ViewController } from 'ionic-angular';

import { Point } from '../../models/home';
import { RealTime } from '../../models/real-time';
import { RealTimeProvider } from '../../providers/real-time/real-time';

@IonicPage()
@Component({
  selector: 'page-real-time',
  templateUrl: 'real-time.html',
})
export class RealTimePage {

  point: Point;
  realTime: RealTime;
  measure: string;

  constructor(
    private navParams: NavParams,
    private viewController: ViewController,
    private realTimeProvider: RealTimeProvider
  ) {
    console.log('constructor RealTimePage');
    this.point = this.navParams.get('point');
  }

  ionViewDidEnter() {
    this.realTimeProvider.get(this.point.id)
      .subscribe(realTime => {
        this.realTime = realTime;
        this.measure = 'temperatura';
      });
  }

  async dismiss() {
    try {
      await this.viewController.dismiss();
    } catch (error) {
      console.error('[RealTimePage] dismiss', error);
    }
  }

}
