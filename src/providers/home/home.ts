import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Home } from '../../models/home';

@Injectable()
export class HomeProvider {

  private static readonly HOME_URL = '/v1/home.json';

  constructor(public http: HttpClient) {
    console.log('constructor HomeProvider');
  }

  get() {
    return this.http.get<Home>(HomeProvider.HOME_URL);
  }

}
