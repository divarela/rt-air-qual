import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { Team } from '../../models/team';

@Injectable()
export class TeamProvider {

  private static readonly TEAM_URL = '/v1/equipes.json';

  constructor(
    private http: HttpClient
  ) {
    console.log('constructor TeamProvider');
  }

  getAll() {
    return this.http.get<Team[]>(TeamProvider.TEAM_URL);
  }

}
