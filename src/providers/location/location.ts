import { Injectable } from '@angular/core';
import { Geolocation } from '@ionic-native/geolocation';

@Injectable()
export class LocationProvider {

  constructor(
    private geolocation: Geolocation
  ) {
    console.log('constructor LocationProvider');
  }

  getCurrentLocation() {
    return this.geolocation.getCurrentPosition({ timeout: 5000 });
  }

}
