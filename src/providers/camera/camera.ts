import { Injectable } from '@angular/core';
import { Camera, CameraOptions, PictureSourceType } from '@ionic-native/camera';

import { ActionSheetProvider } from '../action-sheet/action-sheet';

@Injectable()
export class CameraProvider {

  private options = (sourceType: PictureSourceType): CameraOptions => ({
    quality: 100,
    destinationType: this.camera.DestinationType.DATA_URL,
    encodingType: this.camera.EncodingType.JPEG,
    mediaType: this.camera.MediaType.PICTURE,
    allowEdit: true,
    sourceType,
  })

  constructor(
    private camera: Camera,
    private actionSheetProvider: ActionSheetProvider
  ) {
    console.log('constructor CameraProvider');
  }

  async getPicture() {
    const type = await this.actionSheetProvider.presentPhotoActionSheet();
    const imageData = await this.camera.getPicture(this.options(type));
    return 'data:image/jpeg;base64,' + imageData;
  }

}
