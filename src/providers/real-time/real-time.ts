import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';

import { RealTime } from '../../models/real-time';

@Injectable()
export class RealTimeProvider {

  constructor(
    private http: HttpClient
  ) {
    console.log('constructor RealTimeProvider');
  }

  get(pointId: number) {
    return this.http.get<RealTime>(this.realTimeUrl(pointId));
  }

  private realTimeUrl(pointId: number) {
    return `/v1/tempo_real.json?ponto_id=${pointId}`;
  }

}
