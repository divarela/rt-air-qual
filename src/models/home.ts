export interface Home {
  perfil: Profile;
  pontos: Point[];
}

export interface Profile {
  nome: string;
  equipe: string;
}

export interface Point {
  id: number;
  nome: string;
  latitude: number;
  longitude: number;
  raio: number;
}
