import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SplashScreen } from '@ionic-native/splash-screen';
import { IonicPage, NavController } from 'ionic-angular';

import { LoginProvider } from '../../providers/login/login';

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

  form: FormGroup;
  serverError: string;
  loading: boolean;

  constructor(
    formBuilder: FormBuilder,
    private splashScreen: SplashScreen,
    private navController: NavController,
    private loginProvider: LoginProvider
  ) {
    console.log('constructor LoginPage');

    this.form = formBuilder.group({
      email: ['', Validators.compose([Validators.required, Validators.email])],
      password: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
    });
  }

  ionViewDidLoad() {
    this.splashScreen.hide();
  }

  async createAccount() {
    try {
      await this.navController.push('RegisterPage');
    } catch (error) {
      console.error('[LoginPage] createAccount', error);
    }
  }

  login() {
    this.serverError = '';
    this.loading = true;

    this.loginProvider.login(this.form.value)
      .subscribe(
        () => this.navController.setRoot('MenuPage'),
        error => {
          this.loading = false;
          this.serverError = error.error.error;
        }
      );
  }

}
