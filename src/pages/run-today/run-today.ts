import { Component } from '@angular/core';
import { IonicPage, ViewController } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-run-today',
  templateUrl: 'run-today.html',
})
export class RunTodayPage {

  constructor(
    private viewController: ViewController
  ) {
    console.log('constructor RunTodayPage');
  }

  async dismiss(run?: boolean) {
    try {
      await this.viewController.dismiss({ run });
    } catch (error) {
      console.error('[RunTodayPage] dismiss', error);
    }
  }

}
