import { NgModule } from '@angular/core';
import { BrMaskerModule } from 'brmasker-ionic-3';
import { IonicPageModule } from 'ionic-angular';

import { RegisterPage } from './register';

@NgModule({
  declarations: [
    RegisterPage,
  ],
  imports: [
    IonicPageModule.forChild(RegisterPage),
    BrMaskerModule,
  ],
})
export class RegisterPageModule { }
