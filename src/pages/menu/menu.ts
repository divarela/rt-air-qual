import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';

import { User } from '../../models/user';
import { LoginProvider } from '../../providers/login/login';

@IonicPage()
@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class MenuPage {

  rootPage = 'HomePage';
  user: User;

  constructor(
    private loginProvider: LoginProvider,
    private navController: NavController
  ) {
    this.user = this.loginProvider.user as User;
  }

  openPage(page: string) {
    this.rootPage = page;
  }

  async logOut() {
    this.loginProvider.logout();
    try {
      await this.navController.setRoot('LoginPage');
    } catch (error) {
      console.error('[MenuPage] logOut', error);
    }
  }

}
