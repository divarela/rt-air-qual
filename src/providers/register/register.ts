import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { tap } from 'rxjs/operators';

import { User } from '../../models/user';
import { LoginProvider } from '../login/login';

@Injectable()
export class RegisterProvider {

  private static readonly REGISTER_URL = '/accounts.json';

  constructor(
    private http: HttpClient,
    private loginProvider: LoginProvider
  ) {
    console.log('constructor RegisterProvider');
  }

  post(user: User) {
    return this.http.post<User>(RegisterProvider.REGISTER_URL, user)
      .pipe(
        tap(u => this.loginProvider.user = u)
      );
  }

}
