import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

import { RunTodayPage } from './run-today';

@NgModule({
  declarations: [
    RunTodayPage,
  ],
  imports: [
    IonicPageModule.forChild(RunTodayPage),
  ],
})
export class RunTodayPageModule { }
