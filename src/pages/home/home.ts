import { Component, ElementRef, ViewChild } from '@angular/core';
import { IonicPage, MenuController, ModalController, NavController, TextInput } from 'ionic-angular';
import leaflet from 'leaflet';

import { Point } from '../../models/home';
import { HomeProvider } from '../../providers/home/home';
import { LocationProvider } from '../../providers/location/location';

@IonicPage()
@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
})
export class HomePage {

  @ViewChild('map') mapContainer: ElementRef;
  map: leaflet.Map;
  center: any = [-23.3248631, -51.1710937];
  points: Point[];
  searchedPoints: Point[];
  query: string;

  constructor(
    private navController: NavController,
    private menuController: MenuController,
    private homeProvider: HomeProvider,
    private modalController: ModalController,
    private locationProvider: LocationProvider
  ) {
    console.log('constructor HomePage');
  }

  ionViewDidEnter() {
    this.loadmap();
    this.getPoints();
  }

  search(event: TextInput) {
    if (!Boolean(event.value)) {
      this.searchedPoints = [];
      return;
    }

    const query = event.value.toLocaleLowerCase();
    this.searchedPoints = this.points
      .filter(p => p.nome.toLocaleLowerCase().includes(query))
      .slice(0, 3);
  }

  async setCenterByPoint(point: Point) {
    this.map.panTo([point.latitude, point.longitude]);
    this.query = '';
    try {
      await this.openRealTime(point);
    } catch (error) {
      console.error('[HomePage] setCenterByPoint', error);
    }
  }

  async setCenterByUserLocation() {
    try {
      const userLocation = await this.locationProvider.getCurrentLocation();
      this.map.panTo([ userLocation.coords.latitude, userLocation.coords.longitude ]);
    } catch (error) {
      console.error('[HomePage] setCenterByUserLocation', error);
    }
  }

  async menuToogle() {
    try {
      await this.menuController.toggle();
    } catch (error) {
      console.error('[HomePage] menuToogle', error);
    }
  }

  async runToday() {
    const runTodayModal = this.modalController.create('RunTodayPage', { }, { cssClass: 'inset-modal' });

    runTodayModal.onWillDismiss(this.runTodayModalDismissHandler.bind(this));

    try {
      await runTodayModal.present();
    } catch (error) {
      console.error('[HomePage] runToday', error);
    }
  }

  private loadmap() {
    this.map = leaflet.map('map', { center: this.center, zoomControl: false, zoom: 15 });
    leaflet.tileLayer('http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      attribution: 'www.tphangout.com',
      maxZoom: 18,
    }).addTo(this.map);
  }

  private getPoints() {
    this.homeProvider.get()
      .subscribe(home => {
        this.points = home.pontos;
        this.setPoints();
      });
  }

  private setPoints() {
    // TODO: ADD icone correto
    const icon = leaflet.icon({
      iconUrl: 'https://cdn2.iconfinder.com/data/icons/finance-solid-icons-vol-3/48/106-512.png',
      iconSize: [20, 20],
    });

    this.points.map(p => {
      const marker = leaflet.marker([p.latitude, p.longitude], { icon });
      marker.on('click', () => this.openRealTime(p));
      marker.addTo(this.map);
    });
  }

  private async openRealTime(point: Point) {
    try {
      await this.modalController.create('RealTimePage', { point }, { cssClass: 'inset-modal' }).present();
    } catch (error) {
      console.error('[HomePage] openRealTime', error);
    }
  }

  private async runTodayModalDismissHandler(result: { run?: boolean }) {
    try {
      if (result.run === undefined) {
        return;
      }

      if (result.run) {
        await this.navController.push('RegisterActivityPage');
      }
    } catch (error) {
      console.error('[HomePage] runTodayModalDismissHandler', error);
    }
  }
}
