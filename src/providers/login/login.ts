import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import { tap } from 'rxjs/operators';

import { Login } from '../../models/login';
import { User } from '../../models/user';

@Injectable()
export class LoginProvider {

  private static readonly LOGIN_URL = '/accounts/sign_in.json';

  user?: User;

  constructor(
    private http: HttpClient
  ) {
    console.log('constructor LoginProvider');
  }

  login(login: Login): Observable<User> {
    return this.http.post<User>(LoginProvider.LOGIN_URL, { user: login })
      .pipe(
        tap(user => this.user = user)
      );
  }

  logout() {
    this.user = undefined;
  }

}
