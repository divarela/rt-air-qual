import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { of } from 'rxjs/observable/of';
import { catchError, debounceTime, distinctUntilChanged, map, switchMap, take } from 'rxjs/operators';

import { ValidUser } from '../../models/valid-user';

@Injectable()
export class ServerValidationProvider {

  constructor(
    private http: HttpClient
  ) {
    console.log('constructor ServerValidationProvider');
  }

  emailValid(formControl: FormControl) {
    const error = { unavailable: true };

    return formControl.valueChanges.pipe(
      debounceTime(500),
      distinctUntilChanged(),
      take(1),
      switchMap(email => this.validUser(email)
        .pipe(
          map(res => res.valido ? undefined : error),
          catchError(() => of(error))
        )
      )
    );
  }

  mismatchedPasswords(senha: string, confirmarSenha: string) {
    return (group: FormGroup) => {
      const password = group.controls[senha];
      const confirmPassword = group.controls[confirmarSenha];

      if (password.value !== confirmPassword.value) {
        return { mismatchedPasswords: true };
      }
      return;
    };
  }

  private validUser(email: string) {
    return this.http.get<ValidUser>(
      this.validUserUrl(email)
    );
  }

  private validUserUrl(email: string) {
    return `/v1/valida_email.json?email=${email}`;
  }

}
