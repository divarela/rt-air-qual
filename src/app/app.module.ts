import { HttpClientModule } from '@angular/common/http';
import { ErrorHandler, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { Camera } from '@ionic-native/camera';
import { Geolocation } from '@ionic-native/geolocation';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { Interceptor } from '../auth/interceptor.module';
import { ActionSheetProvider } from '../providers/action-sheet/action-sheet';
import { CameraProvider } from '../providers/camera/camera';
import { HomeProvider } from '../providers/home/home';
import { LocationProvider } from '../providers/location/location';
import { LoginProvider } from '../providers/login/login';
import { RealTimeProvider } from '../providers/real-time/real-time';
import { RegisterProvider } from '../providers/register/register';
import { ServerValidationProvider } from '../providers/server-validation/server-validation';
import { TeamProvider } from '../providers/team/team';

import { MyApp } from './app.component';

@NgModule({
  declarations: [
    MyApp,
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    Interceptor,
    HttpClientModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: ErrorHandler, useClass: IonicErrorHandler },
    LoginProvider,
    ServerValidationProvider,
    TeamProvider,
    Camera,
    ActionSheetProvider,
    CameraProvider,
    RegisterProvider,
    HomeProvider,
    RealTimeProvider,
    Geolocation,
    LocationProvider,
  ],
})
export class AppModule { }
