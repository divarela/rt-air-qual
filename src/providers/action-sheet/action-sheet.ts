import { Injectable } from '@angular/core';
import { ActionSheetController } from 'ionic-angular';

@Injectable()
export class ActionSheetProvider {

  constructor(
    private actionSheetController: ActionSheetController
  ) {
    console.log('constructor ActionSheetProvider');
  }

  presentPhotoActionSheet(): Promise<number> {
    return new Promise((resolve, reject) => {
      const actionSheet = this.actionSheetController.create({
        title: 'Foto',
        buttons: [
          {
            text: 'Galeria',
            icon: 'albums',
            handler: () => {
              resolve(0);
            },
          },
          {
            text: 'Câmera',
            icon: 'camera',
            handler: () => {
              resolve(1);
            },
          },
        ],
      });
      actionSheet.present().catch(reject);
    });
  }

}
