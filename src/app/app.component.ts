import { Component } from '@angular/core';
import { StatusBar } from '@ionic-native/status-bar';
import { Platform } from 'ionic-angular';

@Component({
  templateUrl: 'app.html',
})
export class MyApp {

  rootPage: string;

  constructor(
    private platform: Platform,
    private statusBar: StatusBar
  ) {
    this.initApp()
      .catch(error => console.error('[MyApp] constructor', error));
  }

  async initApp() {
    await this.platform.ready();
    this.statusBar.styleDefault();

    this.rootPage = 'LoginPage';
  }

}
